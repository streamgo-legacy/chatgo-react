const path = require('path');
const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');
const bundleOutputDir = './public';

module.exports = (env) => {
    const isProductionBuild = env && env.production;

    return [{
        entry: './src/chatGo.js',
        mode: 'production',
        output: {
            filename: 'chatGo.js',
            path: path.resolve(bundleOutputDir),
            library: "chatGo",
            // libraryExport: 'window',
            libraryTarget: "var"
        },
        devtool: isProductionBuild ? 'none' : 'source-map',
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: ['babel-loader']
                }
            ],
        },
        resolve: {
            extensions: ["*", ".js", ".jsx"],
        },
        plugins: [
            new Dotenv()
        ]
    }];
};
