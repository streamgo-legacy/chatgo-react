import videoCallStatus from "../../app/enums/videoCallStatus";

export interface UserLoginPayload {
    email: string;
    password: string;
}

export interface UserState {
    token: string;
    firebase_token: string;
    event_key: string;
    status: 'idle' | 'loading' | 'failed';
    token_timestamp?: number | null;
}
export interface InputChangeEventDetail {
    value: any;
    name: string;
}

export interface UserRegisterPayload {
    first_name: string;
    last_name: string;
    organisation: string;
    job_title: string;
    email: string;
    password: string;
    password_confirmation: string;
}

export interface CallStatus {
    status: videoCallStatus;
    groupId?: string;
}

export interface User {
    id: number;
    firstName: string;
    lastName: string;
    jobTitle: string;
    organisation: string;
    profilePicture: string;
    email: string;
    online: boolean;
    // timestamps: { [key: number | string]: string}
    isInit?: boolean;
    callStatus: CallStatus;
    is_sponsor_user: boolean;
    // moderator?: boolean;
}

export interface UserTimestamp{
    [key: number | string]: string
}

export interface UserUpdatePayload {
    first_name: string;
    last_name: string;
    job_title: string;
    profile_picture: string;
    organisation: string;
}

export interface UserSettingsPayload {
    flg_alert_push: boolean;
    flg_mail_freq: 0 | 1 | 2;
    flg_alert_mail: boolean;
    status: {
        status: string;
        manual: boolean;
    }
}

export interface UserPasswordPayload {
    password: string;
    password_confirmation: string;
}