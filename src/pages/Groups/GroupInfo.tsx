import { IonButton, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonTitle, IonToolbar, useIonAlert, IonPage } from "@ionic/react";
import { addCircleOutline, chevronBackOutline } from "ionicons/icons";
import { Fragment } from "react";
import { useHistory, useParams } from "react-router";
import { getDMGroup, userNotBlocked } from "../../app/firebase";
import { useAppSelector } from '../../app/hooks';
import { useUserBlockMutation, useUserUnBlockMutation } from "../../app/services/userApi";
import { selectGroupById } from "../../app/slices/groupSlice";
import { userIdSelector } from '../../app/slices/userSlice';
import getDummyUsers from '../../components/Classes/DummyUsers';
import UserAvatar from "../../components/UserAvatar";
import UserItem from "../../components/UserItem";
interface Props{
    children?: any;
}

interface Group{
    id: string;
    name: string;
    admins: Array<number>;
    directMessage: boolean;
    members: Array<number>;
    meta: {
        lastMessage: string;
        lastSent: Date
    }
    modifiedAt: Date
}

const DUMMY_GROUP: Group = {
    id: "SOME_RANDOM_UID",
    name: 'StremGo Dev Group',
    admins: [0],
    directMessage: false,
    members: [0, 1, 2, 3],
    meta: {
        lastMessage: 'GG boi',
        lastSent: new Date(Date.now() - Math.ceil(Math.random()*3600000))
    },
    modifiedAt: new Date(Date.now() - Math.ceil(Math.random()*3600000))
}

const DUMMY_USERS = getDummyUsers(4);


const GroupInfo: React.FC<Props> = (props: Props) => {
    const parma = useParams<{ id: string }>();
    const group = useAppSelector(selectGroupById(parma.id))
    const history = useHistory();
    const userId = useAppSelector(userIdSelector);
    const [leaveAlertPresent] = useIonAlert();
    const [ block, { isBlockLoading } ] = useUserBlockMutation();
    const [ unblock, { isUnBlockLoading } ] = useUserUnBlockMutation();
    const [ alert ] = useIonAlert();

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar color='primary'>
                    <IonButton slot="start" fill="clear" color='white' className='ion-no-padding'
                        onClick={() => history.goBack()}
                    >
                        <IonIcon icon={chevronBackOutline} />
                        <IonLabel>{group.name}</IonLabel>
                    </IonButton>
                    <IonTitle>Group Info</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent color='light'>
                <IonList className='ion-padding ion-margin-bottom'>
                    <IonItem lines="none">
                        <UserAvatar
                            id={1}
                            firstName={group.name}
                            size={48}
                        />
                        <IonLabel><b>{group.name}</b></IonLabel>
                    </IonItem>
                </IonList>
                <br/>
                <IonLabel className='ion-margin-top ion-padding'>{Object.keys(group.users).length} Participants</IonLabel>
                <IonList className='' lines='inset'>

                 {
                    Object.values(group.users).map(item => (
                         <UserItem
                             id={item.id}
                             user={item}
                             firstName={item.firstName}
                             lastName={item.lastName}
                             organisation={(group.admins.includes(item.id) ? 'Group Admin' : 'Participants') + (item.id === userId ? ` (You)` : '')}
                             profileImage={item.profilePicture}
                             showActions={item.id !== userId}
                             actionObj={ group.admins.includes(userId) ? 
                                 {
                                     buttons: [
                                         {
                                             text: group.admins.includes(item.id) ? `Dismiss As Admin`:'Make Admin'
                                         },
                                         {
                                             text: `Remove ${item.firstName}`
                                         },
                                         {
                                             text: `Message ${item.firstName}`,
                                             handler: () => {
                                                getDMGroup(item.id)
                                                .then(id => {
                                                    history.push(`${process.env.PUBLIC_URL}/group/${id}`)
                                                })
                                            }
                                         },
                                         {
                                             text: 'View Profile',
                                             handler: () => history.push({
                                                 pathname: `${process.env.PUBLIC_URL}/userprofile/${item.id}`,
                                                 state: item,
                                             })
                                         },
                                         { text: 'Cancel', role: 'cancel' }
                                     ]
                                 }
                                 :
                                 {
                                     buttons: userNotBlocked(String(item.id))
                                     ? [
                                         {
                                             text: `Message ${item.firstName}`,
                                             handler: () => {
                                                getDMGroup(item.id)
                                                .then(id => {
                                                    history.push(`${process.env.PUBLIC_URL}/group/${id}`)
                                                })
                                            }
                                         },
                                         {
                                             text: 'View Profile',
                                             handler: () => history.push({
                                                pathname: `${process.env.PUBLIC_URL}/userprofile/${item.id}`,
                                                state: item,
                                            })
                                         },
                                          { 
                                              text: `Block ${item.firstName}`, 
                                              handler: async () => {
                                                const response: {data: { success: boolean } } = await block({
                                                    target: item.id,
                                                })

                                                var message = `Failed to block the ${item.firstName}`;
                                                var header = "Error";
                                                if (response.data.success) {
                                                    message = `Successfully blocked ${item.firstName}`;
                                                    header = "Success";
                                                }

                                                alert({
                                                    header,
                                                    message,
                                                    buttons: [
                                                        'OK',
                                                    ],
                                                });
                                              }
                                         },
                                         { text: 'Cancel', role: 'cancel' }
                                     ]
                                    : [
                                        { 
                                            text: `Unblock ${item.firstName}`, 
                                            handler: async () => {
                                                const response: {data: { success: boolean } } = await unblock({
                                                    target: item.id,
                                                })

                                                var message = `Failed to unblock the ${item.firstName}`;
                                                var header = "Error";
                                                if (response.data.success) {
                                                    message = `Successfully unblocked ${item.firstName}`;
                                                    header = "Success";
                                                }

                                                alert({
                                                    header,
                                                    message,
                                                    buttons: [
                                                        'OK',
                                                    ],
                                                });
                                            }
                                        },
                                       { text: 'Cancel', role: 'cancel' }
                                    ] 
                                 }
                             }
                         />
                     ))
                 }

                 <IonItem role="button" lines='none'>
                     <IonIcon icon={addCircleOutline} color='primary' />
                     <IonLabel color='primary'>Invite More Users</IonLabel>
                 </IonItem>
                </IonList>
                <Fragment>
                     <div className='ion-padding ion-margin-top'>
                         <IonButton expand='block' color='medium'
                             onClick={() => leaveAlertPresent({
                                 header: 'Confrim To Leave This Chat?',
                                 buttons: [
                                     {
                                         text: 'Leave Now',
                                         role: 'destructive',
                                         handler: () => {}
                                     },
                                     {
                                         text: 'Cancel',
                                         role: 'Cancel',
                                         handler: () => {}
                                     },
                                   ],
                             })}
                         >Exit Group</IonButton>
                     </div>
                 </Fragment>
            </IonContent>
        </IonPage>
    )
}


export default GroupInfo;