import {
    IonContent, IonItem,
    IonLabel, IonList, IonPage, useIonActionSheet,
    useIonAlert
} from "@ionic/react";
import { useEffect, useState } from "react";
import { fetchBlockedUserDetails } from "../../app/firebase";
import { useAppSelector } from "../../app/hooks";
import { useUserUnBlockMutation } from "../../app/services/userApi";
import { selectUserSettings } from "../../app/slices/userSlice";
import EmptyContent from "../../components/EmptyContent";
import TabHeader from "../../components/TabHeader";
import UserAvatar from "../../components/UserAvatar";
import { User } from "../Auth/types";

const BlockedUsers: React.FC = props => {
    const [present] = useIonActionSheet();
    const self = useAppSelector(selectUserSettings);
    const [users, setUsers] = useState<Array<User>>([]);
    const [unblock] = useUserUnBlockMutation();
    const [alert] = useIonAlert();

    useEffect(() => {
        if (Object.keys(self.blocked).length > 0) {
            (async () => {
                const updatedUsers = await fetchBlockedUserDetails();
                setUsers(updatedUsers);
            })();
        } else {
            setUsers([]);
        }
    }, [self.blocked]);

    return (
        <IonPage>
            <TabHeader title='Your Blocked Users' back hideUser />
            <IonContent fullscreen>
                <IonList lines="none">
                    {   users && (
                            users.length <= 0 ? <EmptyContent header={`You've not blocked anyone`} message={`If you've blocked someone, they'll be displayed here`} /> :
                                users.map((user) => (
                                    <IonItem
                                        key={user.id}
                                        style={{ cursor: 'pointer' }}
                                        onClick={() =>
                                            present({
                                                buttons: [
                                                    {
                                                        text: `Unblock ${user.firstName}`,
                                                        handler: async () => {
                                                            const response: { data: { success: boolean; }; } = await unblock({
                                                                target: user.id,
                                                            });

                                                            var message = `Failed to unblock the ${user.firstName}`;
                                                            var header = "Error";
                                                            if (response.data.success) {
                                                                message = `Successfully unblocked ${user.firstName}`;
                                                                header = "Success";
                                                            }

                                                            alert({
                                                                header,
                                                                message,
                                                                buttons: [
                                                                    'OK',
                                                                ],
                                                            });
                                                        },
                                                    },
                                                    {
                                                        text: 'Cancel',
                                                        role: 'cancel'
                                                    }
                                                ],
                                            })
                                        }
                                    >
                                        <UserAvatar
                                            id={user.id}
                                            firstName={user.firstName}
                                            img={user.profilePicture}
                                        />
                                        <IonLabel>
                                            <h2>{`${user.firstName}`}</h2>
                                            <p>{`${user.organisation}`}</p>
                                            <br />
                                        </IonLabel>

                                    </IonItem>
                                ))
                    )}
                </IonList>
            </IonContent>
        </IonPage>
    );
};

export default BlockedUsers;