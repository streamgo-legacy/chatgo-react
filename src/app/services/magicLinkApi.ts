import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/dist/query/react";
import { RootState } from "../store";
import { MagicLinkPayload } from "../types/magicLink";

export const magicLinkApi: any = createApi({
    reducerPath: 'userApi',
    baseQuery: fetchBaseQuery({
        baseUrl: String(process.env.REACT_APP_CHATGO_API),
        prepareHeaders: (headers, { getState }) => {
            let token: string = (getState() as RootState).auth.token;
            headers.set('authorization', `Bearer ${token}`);
            headers.set('accept', 'application/json')

            return headers;
        }
    }),
    endpoints: (builder) => ({
        loginWithMagicLink: builder.mutation<any, MagicLinkPayload>({
            query: (body: MagicLinkPayload) => ({
                url: "chat-users/magic-link",
                method: "POST",
                body
            })
        }),
    }),
});

export const { useLoginWithMagicLinkMutation } = magicLinkApi;
