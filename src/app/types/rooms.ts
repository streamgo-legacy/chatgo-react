import { Message } from "../../models/message";
import { User } from "../../pages/Auth/types";

export interface Room {
    id: number;
    name: string;
    order: number;
}

export interface RoomMessage {
    id: String;
    message: String;
    sender: User,
    timestamp: Date;
}

export interface RoomOverview {
    id: number;
    name: string;
    order: number;
    meta: {
        lastMessage: string;
        lastSent: null;
    }
}