export interface EventToggles{
    chat_rooms?: boolean | null;
    direct_messaging?: boolean | null;
    filters?: boolean | null;
    matchmaking?: boolean | null;
}