export interface Question {
    id: number;
    question: string;
    type: string;
    answers: Array<Answer>;
    order: number;
}

export interface Answer{
    id: number;
    answer: string;
    group: string | null;
    opposite: number | null;
}
export interface QuestionChoice {
    [key: string]: {
        id: number,
        value: string,
    }
}

export interface Questions {
    questions: Array<Question>;
}

export interface UserQuestionUpdatePayload {
    id: number;
    answer: Array<number>;
}