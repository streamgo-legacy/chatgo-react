import { User } from "../../pages/Auth/types";

export interface UserSettings {
    missedChatEmail: boolean;
    missedChatFrequency: 0 | 1 | 2;
    pushNotifications: boolean;
    status: {
        manual: boolean;
        status: 'online' | 'offline' | 'hidden';
    };
    isInit?: boolean,
    blocked: {
        [key: string]: number;
    },
    blockedBy: {
        [key: string]: number;
    };
    moderator: boolean
}

export interface UserAnswer{
    id: number;
    answer: Array<number>;
}

export interface UserTimestamp{
    [key: number | string]: string
}

export interface UserState {
    userId: number;
    user: User;
    timestamps: { [key: number | string]: string}
    settings: UserSettings;
    currentChannel: Array<number | string>;
    answers: Array<UserAnswer>;
    // matches: Array<UserMatch>;
    matchmaking: boolean;
}