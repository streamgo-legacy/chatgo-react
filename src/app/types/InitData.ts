export interface InitData{
    mode?: string | null;
    eventKey?: string | null | undefined;
    userToken?: string | null | undefined;
    firebaseToken?: string | null | undefined;
    dmId: string | null | undefined;
    roomId: string | number | null | undefined;
    dmBack?: boolean;
    isAutoAuth: boolean;
    event_name?: string | null;
    themeColors?: {
        colour_1: string,
        colour_2: string
    } | null;
}