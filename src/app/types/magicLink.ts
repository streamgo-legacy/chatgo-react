export interface MagicLinkPayload{
    orgainisation_key: string;
    event_key: string;
    token: string;
}