import { Message } from "../../models/message";
import { User } from "../../pages/Auth/types";

export interface Group {
    id: string,
    name: string,
    directMessage: boolean,
    admins: Array<number>,
    members: Array<number>,
    meta: {
        createdBy: number,
        thumbnail: string,
    },
    modifiedAt: string,
    createdAt: string,
    messages: Array<Message>;
    users: Array<User>;
}