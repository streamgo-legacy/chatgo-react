export interface DolbyLoginResponse {
    access_token: string
    date: Date
    error_message?: string
    moderator: boolean
    referer: string
    role: number
    roomId: string
    unknown_referer: boolean
}