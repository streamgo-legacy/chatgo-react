export interface MagicEvent{
    name: string;
    event_key: string;
    magic_token: string;
    organisation_name?: string;
}

export interface MagicToken{
    event?: string | null;
    org?: string | null;
    token: string;
}