export interface ChatRoom {
  id: number;
  name: string;
  latestMessage: LatestMessage;
  participantCount: number;
  messageCount: number;
}

export interface LatestMessage{
  senderName: string;
  message: string;
}