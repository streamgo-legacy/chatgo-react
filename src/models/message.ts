import { Timestamp } from "@firebase/firestore";
import { User } from "../pages/Auth/types";

export interface Message{
    id: string | null;
    message: string;
    sender: User;
    status: "sent" | "valid" | "invalid" | 'hidden';
    recipient: number | string;
    systemMessage: boolean;
    timestamp: number;
}
export interface MessagePayload{
    id: string | null;
    message: string;
    sender: User;
    status: "sent" | "valid" | "invalid" | 'hidden';
    recipient: number | string;
    systemMessage: boolean;
    timestamp: Timestamp;
}

export interface MessageDisplayItem{
    type: 'divider' | 'message' | 'placeholder';
    message: Message;
}