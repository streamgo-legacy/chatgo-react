import videoCallStatus from '../../app/enums/videoCallStatus';
import { User } from '../../pages/Auth/types';

var chance = require('chance').Chance();

const getDummyUsers = (count: number) => {
    let dummyUsers: Array<User> = [];

    for (var i = 0; i < count; i++) {
        const temp: User = {
            id: i,
            firstName: `${chance.first()}`,
            lastName: `${chance.last()}`,
            jobTitle: `${chance.profession()}`,
            organisation: `${chance.company()}`,
            profilePicture: `https://picsum.photos/400/400?random=${i + 1}`,
            email: `${chance.email()}`,
            online: Math.random() > 0.5,
            callStatus: { status: videoCallStatus.available },
            is_sponsor_user: false
            // timestamps: {},
        }
        dummyUsers.push(temp);
    }
    return dummyUsers;
}

export default getDummyUsers;